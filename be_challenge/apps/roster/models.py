from datetime import timedelta

from django.db import models
from rest_framework.exceptions import ValidationError


class Employee(models.Model):
    """
    An employee at XYZ client.
    """
    CLIENT_LOCATIONS = (
        ('BRISBANE', 'Brisbane'),
        ('MELBOURNE', 'Melbourne'),
        ('SYDNEY', 'Sydney'),
    )

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    location = models.CharField(max_length=30, choices=CLIENT_LOCATIONS,
        blank=True, null=True)

    class Meta:
        verbose_name = "Employee"
        verbose_name_plural = "Employees"

    def __str__(self):
        return "{} {}".format(self.first_name, self.last_name)

    def get_shifts(self):
        return self.shift_set.all().order_by('start_datetime')


class Shift(models.Model):
    """
    A shift available to be assigned to an employee of XYZ client.
    """
    MIN_REST_PERIOD = 10
    MAX_SHIFTS_IN_ROW = 5
    MAX_SHIFTS_IN_WEEK = 5

    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    break_length = models.PositiveSmallIntegerField(default=60)
    employee = models.ForeignKey('roster.Employee', on_delete=models.CASCADE,
        blank=True, null=True)

    class Meta:
        verbose_name = "Shift"
        verbose_name_plural = "Shifts"

    def __str__(self):
        return "{} - {}".format(
            self.start_datetime.strftime('%d/%m/%Y %I:%M %p'),
            self.end_datetime.strftime('%d/%m/%Y %I:%M %p')
        )

    def validate_start_end(self):
        """
        Validate that shift end datetime is after shift start datetime.
        """
        return self.end_datetime > self.start_datetime

    def validate_min_rest(self):
        """
        Validate if shift is a minimum amount of hours apart for previous.
        """
        start_rest_period = self.start_datetime - timedelta(hours=self.MIN_REST_PERIOD)
        shifts_in_rest_period = Shift.objects.filter(
            end_datetime__gt=start_rest_period,
            end_datetime__lt=self.start_datetime,
            employee=self.employee
        )

        if self.id:
            # Exclude object if it has been created
            shifts_in_rest_period = shifts_in_rest_period.exclude(id=self.id)

        return not shifts_in_rest_period.exists()

    def validate_shifts_in_row(self):
        """
        Validate the number of shifts in a row.
        """
        five_days_ago = self.start_datetime - timedelta(days=self.MAX_SHIFTS_IN_ROW)
        num_shifts_in_past_five_days = Shift.objects.filter(
            end_datetime__gt=five_days_ago,
            end_datetime__lt=self.start_datetime,
            employee=self.employee
        ).count()

        five_days_later = self.end_datetime + timedelta(days=self.MAX_SHIFTS_IN_ROW)
        num_shifts_in_next_five_days = Shift.objects.filter(
            start_datetime__gt=self.end_datetime,
            start_datetime__lt=five_days_later,
            employee=self.employee
        ).count()

        return (num_shifts_in_past_five_days < self.MAX_SHIFTS_IN_ROW and
            num_shifts_in_next_five_days < self.MAX_SHIFTS_IN_ROW)

    def validate_shifts_in_week(self):
        """
        Validate the number of shifts per week.
        """
        seven_days_ago = self.start_datetime - timedelta(days=7)
        num_shifts_in_past_week = Shift.objects.filter(
            start_datetime__gt=seven_days_ago,
            start_datetime__lt=self.start_datetime,
            employee=self.employee
        ).count()

        seven_days_later = self.end_datetime + timedelta(days=7)
        num_shifts_in_next_week = Shift.objects.filter(
            start_datetime__gt=self.end_datetime,
            start_datetime__lt=seven_days_later,
            employee=self.employee
        ).count()

        return (num_shifts_in_past_week < self.MAX_SHIFTS_IN_WEEK and
            num_shifts_in_next_week < self.MAX_SHIFTS_IN_WEEK)

    def clean(self):
        """
        Validate on form save.
        """
        if not self.employee:
            return
        if not self.validate_start_end():
            raise ValidationError(f'End datetime needs to be later than start datetime.')
        if not self.validate_min_rest():
            raise ValidationError(f'There needs to be {self.MIN_REST_PERIOD} hours apart from the last shift.')
        if not self.validate_shifts_in_row():
            raise ValidationError(f"There can't be more than {self.MAX_SHIFTS_IN_ROW} shifts in a row.")
        if not self.validate_shifts_in_week():
            raise ValidationError(f"There can't be more than {self.MAX_SHIFTS_IN_WEEK} shifts in a week.")

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)


class CSVFile(models.Model):
    file = models.FileField(upload_to='csvs/')
    uploaded_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "CSV File"
        verbose_name_plural = "CSV File"
        abstract = True

    def __str__(self):
        return self.file.name


class EmployeesCSVFile(CSVFile):

    class Meta:
        verbose_name = "Employees CSV File"
        verbose_name_plural = "Employees  CSV File"


class ShiftsCSVFile(CSVFile):

    class Meta:
        verbose_name = "Shifts CSV File"
        verbose_name_plural = "Shifts  CSV File"
