from rest_framework import serializers

from .models import Employee, Shift


class EmployeeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Employee
        fields = ("first_name", "last_name", "location")


class ShiftSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shift
        fields = ("start_datetime", "end_datetime",
        		  "break_length", "employee")
