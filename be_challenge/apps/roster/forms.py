import datetime

from django import forms
from django.conf import settings

from .models import Employee, Shift


class EmployeeForm(forms.ModelForm):

    class Meta:
        model = Employee
        fields = ('first_name', 'last_name', 'location')


class ShiftForm(forms.ModelForm):
    start_date = forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)
    start_time = forms.TimeField(input_formats=settings.TIME_INPUT_FORMATS)
    end_time = forms.TimeField(input_formats=settings.TIME_INPUT_FORMATS)
    start_datetime = forms.DateTimeField(required=False)
    end_datetime = forms.DateTimeField(required=False)

    class Meta:
        model = Shift
        fields = ('start_date', 'start_time', 'end_time', 'start_datetime',
                  'end_datetime', 'break_length', 'employee')

    def clean_start_datetime(self):
        if self.errors:
            return
        # Transform start date and time into a datetime
        start_date = self.cleaned_data.get('start_date')
        start_time = self.cleaned_data.get('start_time')
        return datetime.datetime.combine(start_date, start_time)

    def clean_end_datetime(self):
        if self.errors:
            return
        # Transform end time into a datetime
        start_time = self.cleaned_data.get('start_time')
        end_time = self.cleaned_data.get('end_time')
        date = self.cleaned_data.get('start_datetime')

        if end_time < start_time:
            date += datetime.timedelta(days=1)

        return datetime.datetime.combine(date, end_time)
