from celery.app import shared_task


@shared_task
def optimise_shifts():
    """
    This is where optimisation engine would be called.
    For now, we just return a string.
    """
    return "Shifts successfully optimised."
