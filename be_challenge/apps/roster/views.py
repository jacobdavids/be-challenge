from rest_framework import generics, viewsets
from rest_framework.permissions import IsAuthenticated

from django.http import JsonResponse
from django.views import View

from .models import Employee, Shift
from .serializers import EmployeeSerializer, ShiftSerializer
from .tasks import optimise_shifts


class EmployeeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows employees to be viewed or edited.
    """
    queryset = Employee.objects.all().order_by('last_name')
    serializer_class = EmployeeSerializer
    permission_classes = (IsAuthenticated,)


class ShiftViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows shifts to be viewed or edited.
    """
    queryset = Shift.objects.all().order_by('start_datetime')
    serializer_class = ShiftSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        If GET parameter 'employee_id' was passed, filter the shifts for that employee.
        """
        employee_id = self.request.GET.get('employee_id')
        if employee_id:
            return Shift.objects.filter(employee=employee_id).order_by('start_datetime')
        return super().get_queryset()


class RunShiftOptimiser(View):
    """
    View to start shift optimisation external process via Celery.
    """
    def get(self, request, *args, **kwargs):
        optimise_shifts.delay()
        return JsonResponse({'message': 'Shift optimisation is processing...'})
