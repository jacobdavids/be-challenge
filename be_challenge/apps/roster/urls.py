from rest_framework import routers

from django.contrib import admin
from django.urls import path, include

from .views import EmployeeViewSet, ShiftViewSet, RunShiftOptimiser


router = routers.DefaultRouter()
router.register(r'employees', EmployeeViewSet)
router.register(r'shifts', ShiftViewSet)

urlpatterns = [
	path('', include(router.urls)),
	path('shift_optimiser/', RunShiftOptimiser.as_view()),
]
