import csv, datetime

from django.conf import settings

from .forms import EmployeeForm, ShiftForm
from .models import Employee, Shift


def import_employees_csv(file_path):
    """
    Return dict containing import status and message.
    Read, validate and import data from an employees CSV file.
    """
    column_headers = {
        'first_name': 'First Name',
        'last_name': 'Last Name'
    }

    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for line_count, row in enumerate(csv_reader, start=1):
            if line_count == 1:
                for idx, column_header in enumerate(column_headers):
                    if column_headers[column_header] != row[idx]:
                        # Invalid column headers
                        return {
                            'status': 'error',
                            'message': 'Column headers are invalid: Found \'{}\' instead of \'{}\'.'.format(
                                row[idx], column_headers[column_header])
                        }
            else:
                data = {}
                for idx, field in enumerate(column_headers.keys()):
                    data[field] = row[idx]
                form = EmployeeForm(data)
                if form.is_valid():
                    # Create instance in database
                    form.save()
                else:
                    return {
                        'status': 'error',
                        'message': 'Error in row {}: {}'.format(
                            line_count, form.errors)
                    }
    return {
        'status': 'success',
        'message': 'CSV file imported successfully.'
    }


def import_shifts_csv(file_path):
    """
    Return dict containing import status and message.
    Read, validate and import data from a shifts CSV file.
    """
    column_headers = {
        'start_date': 'Date',
        'start_time': 'Start',
        'end_time': 'End',
        'break_length': 'Break'
    }
    
    with open(file_path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        for line_count, row in enumerate(csv_reader, start=1):
            if line_count == 1:
                for idx, column_header in enumerate(column_headers):
                    if column_headers[column_header] != row[idx]:
                        # Invalid column headers
                        return {
                            'status': 'error',
                            'message': 'Column headers are invalid: Found \'{}\' instead of \'{}\'.'.format(
                                row[idx], column_headers[column_header])
                        }
            else:
                data = {}
                for idx, field in enumerate(column_headers.keys()):
                    data[field] = row[idx]
                form = ShiftForm(data)
                if form.is_valid():
                    # Create instance in database
                    form.save()
                else:
                    return {
                        'status': 'error',
                        'message': 'Error in row {}: {}'.format(
                            line_count, form.errors)
                    }
    return {
        'status': 'success',
        'message': 'CSV file imported successfully.'
    }
