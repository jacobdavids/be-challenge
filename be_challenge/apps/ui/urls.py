from django.contrib import admin
from django.urls import path, include

from .views import HomeView, ImportEmployeesCSVView, ImportShiftsCSVView


import_csv_urls = [
    path('employees/', ImportEmployeesCSVView.as_view(), name='employees'),
    path('shifts/', ImportShiftsCSVView.as_view(), name='shifts'),
]


urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('import_csv/', include((import_csv_urls, 'roster'), namespace='import_csv')),
]