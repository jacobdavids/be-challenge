from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView

from .forms import UploadCSVFileForm
from be_challenge.apps.roster.utils import import_employees_csv, import_shifts_csv
from be_challenge.apps.roster.models import EmployeesCSVFile, ShiftsCSVFile


class HomeView(TemplateView):
    template_name = 'index.html'


class ImportEmployeesCSVView(FormView):
    form_class = UploadCSVFileForm
    template_name = 'import_employees_csv.html'

    def form_valid(self, form):
        file = form.cleaned_data.get('file')
        csv_file = EmployeesCSVFile(file=file)
        csv_file.save()

        result = import_employees_csv(csv_file.file.file.name)

        # Display import result message to user
        if result['status'] == 'success':
            messages.success(
                self.request,
                result['message'],
                extra_tags='alert alert-success safe'
            )
        else:
            messages.error(
                self.request,
                result['message'],
                extra_tags='alert alert-danger safe'
            )
        return HttpResponseRedirect(reverse_lazy('home'))

    def form_invalid(self, form):
        # Display error message
        messages.error(
            self.request,
            "Error: Please review below and re-submit.",
            extra_tags='alert alert-danger'
        )
        return super().form_invalid(form)


class ImportShiftsCSVView(FormView):
    form_class = UploadCSVFileForm
    template_name = 'import_shifts_csv.html'

    def form_valid(self, form):
        file = form.cleaned_data.get('file')
        csv_file = ShiftsCSVFile(file=file)
        csv_file.save()

        result = import_shifts_csv(csv_file.file.file.name)

        # Display import result message to user
        if result['status'] == 'success':
            messages.success(
                self.request,
                result['message'],
                extra_tags='alert alert-success safe'
            )
        else:
            messages.error(
                self.request,
                result['message'],
                extra_tags='alert alert-danger safe'
            )
        return HttpResponseRedirect(reverse_lazy('home'))

    def form_invalid(self, form):
        # Display error message
        messages.error(
            self.request,
            "Error: Please review below and re-submit.",
            extra_tags='alert alert-danger'
        )
        return super().form_invalid(form)
