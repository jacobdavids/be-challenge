# Biarri EMI Challenge

### Notes
See here for my notes on the challenge: https://docs.google.com/document/d/177bXpWyuwBJ0_ZpWOfTBlxHvHJPRZCiATM8uzZzWzU4

### Running Celery & RabbitMQ
- RabbitMQ server: `rabbitmq-server` (will need to install)
- Celery worker: `celery -A be_challenge worker -l info`
